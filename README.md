# Container for Todo project,

OpenClassrooms Backend/Symfony Developer path, project #8

## Instructions

- install docker
- clone on computer
- cd to folder
- (OPTIONAL clone Todo project into container folder) https://gitlab.com/DamienAdam/octodo.git  
- FOR BLACKFIRE create .env file and fill blackfire env values with your blackfire account keys
- docker-compose build
- docker-compose up

## NOTE :  

### Blackfire

Blackfire conflicts with Xdebug, Xdebug is installed with the php image but not activated (it can be activated by your IDE though)

If you want to use phpunit cli (which depends on Xdebug) you have to comment the blackfire image in docker-compose.yml and activate Xdebug in 'docker/php/files/etc/php/7.2/mods-available/xdebug.ini'

Once done, re-build docker `docker-compose build && docker-compose up`

### Docker

Recently docker moved `docker-compose` to its main `docker` cli but it appears that the build has still some varying behaviours from the original `docker-compose`, it is recommended to use the former for this project